# cpp-dojo

C++ Training with examples written by the cpp-dojo community

# Forge Platforms

* https://github.com/hwpplayer1/cpp-dojo

* https://gitlab.com/hwpplayer1/cpp-dojo

* https://git.sr.ht/~mertgor/cpp-dojo

* https://git.vern.cc/hwpplayer1/cpp-dojo

* https://git.disroot.org/hwpplayer1/cpp-dojo

* https://codeberg.org/hwpplayer1/cpp-dojo

* https://git.truvalinux.org.tr/hwpplayer1/cpp-dojo

* and your local reposıtory on your own computer

# License

C++ Training with examples written by the cpp-dojo community

Copyright (C) 2023-2024 Mert Gör and contributors

Copyright (C) 2023-2024 Masscollabs Services

Copyright (C) 2023-2024 Mass Collaboration Labs and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Feel free to send an email for your questions to mertgor at masscollabs dot xyz
